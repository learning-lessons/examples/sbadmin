import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BlankComponent } from './pages/blank/blank.component';


const routes: Routes = [
  { path: '', redirectTo: 'blank', pathMatch: 'full' },
  { path: "blank", component: BlankComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
